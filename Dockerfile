FROM resin/armv7hf-systemd:jessie

ENV INITSYSTEM on

RUN apt-get update && apt-get install -y usbutils usb-modeswitch usb-modeswitch-data wvdial

ADD init.sh /init.sh

ADD wvdial.conf /etc/wvdial.conf

CMD /init.sh
